#ifndef DUNE_SUBGRIDTOOLS_HH
#define DUNE_SUBGRIDTOOLS_HH

/** \file
* \brief SubGrid tool classes for interaction with the hostgrid
*/

#include <vector>

#include <dune/disc/shapefunctions/lagrangeshapefunctions.hh>

#include <dune/disc/functions/functions.hh>

namespace Dune {




//! \todo Please doc me !
template<class GridType, class Vector>
class SubGridP1Interpolator
{
    public:
        typedef typename GridType::template Codim<0>::Entity Element;
        typedef typename GridType::HostGridType HostGridType;
        typedef typename HostGridType::template Codim<0>::Entity HostElement;

        enum {dim = GridType::dimension};


        /** \brief Interpolate subgrid P1 function to hostgrid
        *
        * This is a shortcut for creating an (temporary) interpolator object and
        * handing it to the transfer method in the subgrid
        */
        static void interpolate(const GridType& _grid, const Vector& _subGridFunction, Vector& _hostGridFunction)
        {
            typedef SubGridP1Interpolator<GridType, Vector> MyType;

            MyType interpolator(_grid, _subGridFunction, _hostGridFunction);
            _grid.template transfer<MyType>(interpolator);
        }


        //! \todo Please doc me !
        SubGridP1Interpolator(const GridType& _grid, const Vector& _subGridFunction, Vector& _hostGridFunction) :
            grid(_grid),
            hostgrid(_grid.getHostGrid()),
            subGridFunction(_subGridFunction),
            hostGridFunction(_hostGridFunction)
        {}


        //! Prepare hostgrid vector
        void pre()
        {
            processed.resize(hostgrid.size(dim), false);
            hostGridFunction.resize(hostgrid.size(dim));
            hostGridFunction = 0.0;
        }


        //! \todo Please doc me !
        void transfer(const Element& element, const HostElement& hostElement)
        {
            const Dune::LagrangeShapeFunctionSet<double, double, dim>& subGridBaseSet
                = Dune::LagrangeShapeFunctions<double, double, dim>::general(element.type(), 1);
            const int subGridBaseSetSize = subGridBaseSet.size();

            for (int i=0; i<hostElement.subEntities(dim); ++i)
            {
                int hgIndex = hostgrid.leafIndexSet().template subIndex<dim>(hostElement, i);
                if (not(processed[hgIndex]))
                {
                    FieldVector<double, dim> local = element.geometry().local(hostElement.geometry()[i]);
                    for (int j=0; j<subGridBaseSetSize; ++j)
                    {
                        int sgIndex = grid.leafIndexSet().template subIndex<dim>(element, j);
                        typename Vector::block_type interpolatedValue = subGridFunction[sgIndex];
                        interpolatedValue *= subGridBaseSet[j].evaluateFunction(0, local);
                        hostGridFunction[hgIndex] += interpolatedValue;
                    }
                    processed[hgIndex] = true;
                }
            }
            return;
        }


        //! does nothing
        void post()
        {}


    private:
        const GridType& grid;
        const HostGridType& hostgrid;

        //! \todo Please doc me !
        std::vector<bool> processed;

        //! \todo Please doc me !
        const Vector& subGridFunction;

        //! \todo Please doc me !
        Vector& hostGridFunction;
};




//! \todo Please doc me !
template<class GridType, class Vector>
class SubGridP1Restrictor
{
    public:
        typedef typename GridType::template Codim<0>::Entity Element;
        typedef typename GridType::HostGridType HostGridType;
        typedef typename HostGridType::template Codim<0>::Entity HostElement;

        enum {dim = GridType::dimension};


        /** \brief Restrict hostgrid P1 functional to subgrid
        *
        * This is a shortcut for creating an (temporary) restrictor object and
        * handing it to the transfer method in the subgrid
        */
        static void restrict(const GridType& _grid, const Vector& _hostGridFunction, Vector& _subGridFunction)
        {
            typedef SubGridP1Restrictor<GridType, Vector> MyType;

            MyType restrictor(_grid, _hostGridFunction, _subGridFunction);
            _grid.template transfer<MyType>(restrictor);
        }


        //! \todo Please doc me !
        SubGridP1Restrictor(const GridType& _grid, const Vector& _hostGridFunction, Vector& _subGridFunction) :
            grid(_grid),
            hostgrid(_grid.getHostGrid()),
            hostGridFunction(_hostGridFunction),
            subGridFunction(_subGridFunction)
        {}


        //! Prepare subgrid vector
        void pre()
        {
            processed.resize(hostgrid.size(dim), false);
            subGridFunction.resize(grid.size(dim));
            subGridFunction = 0.0;
        }


        //! \todo Please doc me !
        void transfer(const Element& element, const HostElement& hostElement)
        {
            const Dune::LagrangeShapeFunctionSet<double, double, dim>& subGridBaseSet
                = Dune::LagrangeShapeFunctions<double, double, dim>::general(element.type(), 1);
            const int subGridBaseSetSize = subGridBaseSet.size();

            for (int i=0; i<hostElement.subEntities(dim); ++i)
            {
                int hgIndex = hostgrid.leafIndexSet().template subIndex<dim>(hostElement, i);
                if (not(processed[hgIndex]))
                {
                    FieldVector<double, dim> local = element.geometry().local(hostElement.geometry()[i]);
                    for (int j=0; j<subGridBaseSetSize; ++j)
                    {
                        int sgIndex = grid.leafIndexSet().template subIndex<dim>(element, j);
                        subGridFunction[sgIndex] += subGridBaseSet[j].evaluateFunction(0, local) * hostGridFunction[hgIndex];
                    }
                    processed[hgIndex] = true;
                }
            }
            return;
        }


        //! does nothing
        void post()
        {}


    private:

        const GridType& grid;
        const HostGridType& hostgrid;

        //! \todo Please doc me !
        std::vector<bool> processed;

        //! \todo Please doc me !
        const Vector& hostGridFunction;

        //! \todo Please doc me !
        Vector& subGridFunction;
};

/**
 * \brief HostGrid function obtained/interpolated from a SubGrid function
 *
 * This class yields the given SubGridFunction embedded in the space of HostGridFunctions with \f$ f_{text{host}}(x)=0 \forall x\in\Omega_{\text{host}}\setminus\Omega_{\text{sub}} \f$.
 *
 * \tparam SubGridType GridType on which the given (sub-) GridFunction is defined
 * \tparam RT the returntype
 * \tparam m the dimension of the range
 */
template <class SubGridType, class RT, int m=1>
class HostgridLeafFunction:
    virtual public Dune::GridFunctionGlobalEvalDefault<typename SubGridType::HostGridType::LeafGridView, RT, m>
{
    public:
        typedef Dune::GridFunction<SubGridType, RT, m> SubGridFunctionType;
        typedef typename SubGridType::HostGridType HostGridType;

        typedef typename HostGridType::ctype DT;

        typedef typename HostGridType::template Codim<0>::Entity HostGridEntity;

        enum {n = SubGridType::dimension};

        HostgridLeafFunction(const SubGridType& subgrid, const SubGridFunctionType& f_sub):
            Dune::GridFunctionGlobalEvalDefault<typename HostGridType::LeafGridView, RT, m>(subgrid.getHostGrid().leafView()),
            subgrid_(subgrid),
            subgridFunction_(f_sub)
        {}

        /** \brief evaluate single component comp in the hostgrid entity host_ntt at local coordinates x
        *  Evaluate the function in an entity at local coordinates.
        *  @param[in]  comp   number of component to be evaluated
        *  @param[in]  host_ntt      reference to hostgrid entity of codimension 0
        *  @param[in]  x     point in local coordinates of the reference element of host_ntt
        *  \return            value of the component
        */
        virtual RT evallocal (int comp, const HostGridEntity& host_ntt, const Dune::FieldVector<DT,n>& x) const
        {
            HostGridEntity host_ntt_ptr(host_ntt);

            Dune::FieldVector<DT,n> xi = x;

            while (!subgrid_.template contains<0>(host_ntt_ptr) and host_ntt_ptr.level()!=0)
            {
                xi = host_ntt_ptr.geometryInFather().global(xi);
                host_ntt_ptr = host_ntt_ptr.father();
            }

            // If x is outside the domain covered by the subgrid, the hostgridfunction evaluates to zero
            if (!subgrid_.template contains<0>(host_ntt_ptr))
                return 0;
            else
                return subgridFunction_.evallocal(comp, *subgrid_.template getSubGridEntity<0>(host_ntt_ptr), xi);
        }

        //! evaluate all components  in the entity host_ntt at local coordinates x
        /*! Evaluates all components of a function at once.
          @param[in]  host_ntt      reference to grid entity of codimension 0
          @param[in]  x     point in local coordinates of the reference element of e
          @param[out] y      vector with values to be filled
         */
        virtual void evalalllocal (const HostGridEntity& host_ntt, const Dune::FieldVector<DT,n>& x, Dune::FieldVector<RT,m>& y) const
        {
            HostGridEntity host_ntt_ptr(host_ntt);

            Dune::FieldVector<DT,n> xi = x;

            while (!subgrid_.template contains<0>(host_ntt_ptr) and host_ntt_ptr.level()!=0)
            {
                xi = host_ntt_ptr.geometryInFather().global(xi);
                host_ntt_ptr = host_ntt_ptr.father();
            }

            // If x is outside the domain covered by the subgrid the hostgridfunction evaluates to zero
            if (!subgrid_.template contains<0>(host_ntt_ptr))
                y = 0.0;
            else
                subgridFunction_.evalalllocal(*subgrid_.template getSubGridEntity<0>(host_ntt_ptr), xi, y);

        }
    private:
        const SubGridType& subgrid_;

        const SubGridFunctionType& subgridFunction_;

};

} // namespace Dune

#endif
